package com.epam.ld.module2.testing.template;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * The type Template engine.
 */
public class TemplateEngine {
    static final String CUSTOMER_NAME_PLACEHOLDER = "#{CustomerName}";
    static final String ORDER_ID_PLACEHOLDER = "#{OrderId}";
    static final String NUMBER_OF_DELIVERY_DAYS_PLACEHOLDER = "#{NumberOfDeliveryDays}";
    static final String COMPANY_NAME_PLACEHOLDER = "#{CompanyName}";

    /**
     * Generate message string.
     *
     * @param template               the template
     * @param placeholdersWithValues the placeholders with values
     * @return the string
     */
    public String generateMessage(Template template, Map<String, Object> placeholdersWithValues) {
        if (ObjectUtils.isEmpty(template) || MapUtils.isEmpty(placeholdersWithValues)) {
            throw new IllegalArgumentException("Template and templateModel must not be null/empty.");
        }
        if (!placeholdersWithValues.keySet().containsAll(Arrays.asList(CUSTOMER_NAME_PLACEHOLDER, ORDER_ID_PLACEHOLDER, NUMBER_OF_DELIVERY_DAYS_PLACEHOLDER,
                COMPANY_NAME_PLACEHOLDER))) {
            throw new IllegalArgumentException("Not all values of placeholders are presented.");
        }
        String message = template.getContent();
        for (Map.Entry<String, Object> entry : placeholdersWithValues.entrySet()) {
            message = message.replace(
                    entry.getKey(), ObjectUtils.isEmpty(entry.getValue()) ? "" : entry.getValue().toString()
            );
        }
        return message;
    }

    /**
     * Generate message string.
     *
     * @param template               the template
     * @param txtFilePath the placeholders from file
     * @return the string
     * @throws IOException the exception
     */
    public String generateMessage(Template template, Path txtFilePath) throws IOException {
        Map<String, Object> placeholdersWithValues = getPlaceholdersWithValuesFromTxtFile(txtFilePath);
        return generateMessage(template, placeholdersWithValues);
    }

    /**
     * return placeholders with values from txt file
     *
     * @param txtFilePath the placeholders from file
     * @return Map the placeholders
     * @throws IOException the exception
     */
    public Map<String, Object> getPlaceholdersWithValuesFromTxtFile(Path txtFilePath) throws IOException {
        if (null == txtFilePath) {
            throw new IllegalArgumentException("File path can not be null.");
        }
        if (!Files.isRegularFile(txtFilePath)) {
            throw new IllegalArgumentException("This is not a file.");
        }
        if (!txtFilePath.getFileName().toString().toLowerCase().endsWith(".txt")) {
            throw new IllegalArgumentException("File extension is not txt.");
        }
        String[] lines = Files.readAllLines(txtFilePath).toArray(new String[0]);
        int length = lines.length;
        if (length % 2 != 0) {
            throw new IllegalArgumentException("The last placeholder doesn't have value.");
        }
        Map<String, Object> result = new HashMap<>();
        for (int i = 0; i < length; i += 2) {
            String placeHolder = lines[i];
            if (StringUtils.isBlank(placeHolder)) {
                throw new IllegalArgumentException("Placeholder is missed in row " + i);
            }
            String value = lines[i + 1];
            result.put(placeHolder, value);
        }
        return result;
    }

}
