package com.epam.ld.module2.testing;

import com.epam.ld.module2.testing.template.Template;
import com.epam.ld.module2.testing.template.TemplateEngine;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

    /**
     * The entry point.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
        try {
            if (args.length == 2) {
                processFiles(args[0], args[1]);
            } else {
                processConsole();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void processConsole() {
        Scanner scanner = new Scanner(System.in);
        Map<String, Object> placeholders = new HashMap<>();
        Client client = new Client("client@mal.com");
        Template template = new Template();
        TemplateEngine templateEngine = new TemplateEngine();
        MailServer mailServer = new MailServer();
        Messenger messenger = new Messenger(mailServer, templateEngine);

        System.out.println("###################################### TEMPLATE ######################################");
        System.out.println(template.getContent());
        System.out.println("#################################### END TEMPLATE ####################################");
        System.out.println(System.lineSeparator());

        String input = "";
        while (!"s".equalsIgnoreCase(input)) {
            System.out.println("Enter placeholders and values in format:  #{Name}:Bob   ('s' to send message)");
            input = scanner.nextLine();
            if (input.equals("s")) {
                break;
            }
            int index = input.indexOf(":");
            if (index == -1) {
                System.out.println("Invalid format. Look at an example: #{Name}:Bob");
            } else {
                placeholders.put(input.substring(0, index), input.substring(index + 1));
            }
        }
        System.out.println("\n\n");
        String result = messenger.sendMessage(client, template, placeholders);
        System.out.println(result);

        System.out.println("\n\nPress enter to finish");
        scanner.nextLine();
        scanner.close();
    }

    private static void processFiles(String input, String output) throws IOException {
        Client client = new Client("client@mal.com");
        Template template = new Template();
        TemplateEngine templateEngine = new TemplateEngine();
        MailServer mailServer = new MailServer();
        Messenger messenger = new Messenger(mailServer, templateEngine);
        Scanner scanner = new Scanner(System.in);

        Map<String, Object> placeholders = templateEngine.getPlaceholdersWithValuesFromTxtFile(Path.of(input));
        String result = messenger.sendMessage(client, template, placeholders);
        Files.write(Path.of(output), Collections.singleton(result));

        System.out.println("Press enter to finish");
        scanner.nextLine();
        scanner.close();
    }
}
