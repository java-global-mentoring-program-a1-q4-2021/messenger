package com.epam.ld.module2.testing.template;

/**
 * The type Template.
 */
public class Template {
    private static final String SOME_CONTENT = "Hello #{CustomerName}, \n" +
            "\n" +
            "Thank you for your purchase! Your #{OrderId} will be shipped within #{NumberOfDeliveryDays} day(s)." +
            "\n" +
            "We will send you an email as soon as your parcel is on its way.\n" +
            "\n" +
            "Thank you for your purchase, \n" +
            "\n" +
            "Best Regards, \n" +
            "The #{CompanyName} sales team";

    public String getContent() {
        return SOME_CONTENT;
    }
}
