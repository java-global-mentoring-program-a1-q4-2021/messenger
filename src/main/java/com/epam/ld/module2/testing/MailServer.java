package com.epam.ld.module2.testing;

/**
 * Mail server class.
 */
public class MailServer {

    /**
     * Send notification.
     *
     * @param addresses      the addresses
     * @param messageContent the message content
     * @return result        the string
     */
    public String send(String addresses, String messageContent) {
        return "==========================MESSAGE HAS BEEN SUCCESSFULLY SENT==========================" +
                "\n\n" +
                "TO: " + addresses +
                "\n" +
                "BODY: " +
                "\n" +
                messageContent +
                "\n\n" +
                "=========================================================================================";
    }
}
