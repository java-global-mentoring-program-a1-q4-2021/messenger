package com.epam.ld.module2.testing;


import com.epam.ld.module2.testing.template.Template;
import com.epam.ld.module2.testing.template.TemplateEngine;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Map;

/**
 * The type Messenger.
 */
public class Messenger {
    private final MailServer mailServer;
    private final TemplateEngine templateEngine;

    /**
     * Instantiates a new Messenger.
     *
     * @param mailServer     the mail server
     * @param templateEngine the template engine
     */
    public Messenger(MailServer mailServer,
                     TemplateEngine templateEngine) {
        this.mailServer = mailServer;
        this.templateEngine = templateEngine;
    }

    /**
     * Send message.
     *
     * @param client the client
     * @param template the template
     * @param placeholders the placeholders
     * @return result the string
     */
    public String sendMessage(Client client, Template template, Map<String, Object> placeholders) {
        String messageContent =
                templateEngine.generateMessage(template, placeholders);
        return mailServer.send(client.getAddresses(), messageContent);
    }

    /**
     * Send message.
     *
     * @param client the client
     * @param template the template
     * @param fileWithPlaceholders the file
     * @return result the string
     * @throws IOException the exception
     */
    public String sendMessage(Client client, Template template, Path fileWithPlaceholders) throws IOException {
        String messageContent =
                templateEngine.generateMessage(template, fileWithPlaceholders);
        return mailServer.send(client.getAddresses(), messageContent);
    }
}