package com.epam.ld.module2.testing.template;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import static com.epam.ld.module2.testing.template.TemplateEngine.*;
import static org.junit.jupiter.api.Assertions.*;

class TemplateEngineTest {
    private Template template;
    private TemplateEngine templateEngine;

    private static Stream<Arguments> provideATemplateModelWithMissingSomePlaceholders() {
        Map<String, Object> model = new HashMap<>();
        model.put(CUSTOMER_NAME_PLACEHOLDER, "Bob Stuart");
        model.put(ORDER_ID_PLACEHOLDER, "ORDER-23");
        return Stream.of(Arguments.of(model));
    }

    private static Stream<Arguments> provideATemplateModelWithExtraPlaceholders() {
        Map<String, Object> model = new HashMap<>();
        model.put(CUSTOMER_NAME_PLACEHOLDER, "Stuart Bob");
        model.put(ORDER_ID_PLACEHOLDER, "ORDER-23");
        model.put(NUMBER_OF_DELIVERY_DAYS_PLACEHOLDER, "3");
        model.put(COMPANY_NAME_PLACEHOLDER, "APPLE INC");
        model.put("#{UnknownPlaceHolder}", "some value");
        model.put("AnotherUnknownPlaceHolder", "another some value");
        return Stream.of(Arguments.of(model));
    }

    @BeforeEach
    public void setUp() {
        template = new Template();
        templateEngine = new TemplateEngine();
    }

    @Test
    @DisplayName("Should successfully generate message")
    void shouldSuccessfullyGenerateMessage() {
        Map<String, Object> model = new HashMap<>();
        model.put(CUSTOMER_NAME_PLACEHOLDER, "Bob");
        model.put(ORDER_ID_PLACEHOLDER, "#{ORDER-12345}");
        model.put(NUMBER_OF_DELIVERY_DAYS_PLACEHOLDER, 2);
        model.put(COMPANY_NAME_PLACEHOLDER, "APPLE INC");
        testSuccessfullyGenerateMessage(model);
    }

    @Disabled("Do not run in dev environment")
    @Test
    void testOnDev() {

    }

    @ParameterizedTest
    @MethodSource("provideATemplateModelWithExtraPlaceholders")
    @DisplayName("Should successfully generate message ignoring extra placeholders")
    void shouldSuccessfullyGenerateMessageIgnoringExtraPlaceholders(Map<String, Object> model) {
        testSuccessfullyGenerateMessage(model);
    }

    @ParameterizedTest
    @ArgumentsSource(TemplateEngineGenerateMessageNullArgumentsProvider.class)
    @DisplayName("Should throw an exception when args are null or empty")
    void shouldThrowAnExceptionWhenArgsAreNullOrEmpty(Template template, Map<String, Object> model) {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> templateEngine.generateMessage(template, model));
        assertEquals("Template and templateModel must not be null/empty.", exception.getMessage());
    }

    @ParameterizedTest
    @MethodSource("provideATemplateModelWithMissingSomePlaceholders")
    @DisplayName("Should throw an exception if at least one placeholder is not replaced")
    void shouldThrowAnExceptionIfAtLeastOnePlaceholderIsNotReplaced(Map<String, Object> model) {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> templateEngine.generateMessage(template, model));
        assertEquals("Not all values of placeholders are presented.", exception.getMessage());
    }

    @TestFactory
    Collection<DynamicTest> testGettingOfPlaceholdersWithValuesFromTxtFile(@TempDir Path tempDir) throws IOException {
        Path csvFile = tempDir.resolve("file3.csv");
        Path file1 = tempDir.resolve("file1.txt");
        Files.write(file1, Arrays.asList("placeholder1", "value1", "placeholder2"));
        Path file2 = tempDir.resolve("file2.txt");
        Files.write(file2, Arrays.asList("placeholder1", "value1", "", "value2"));
        Path file3 = tempDir.resolve("file3.txt");
        Files.write(file3, Arrays.asList("placeholder1", "value1", "placeholder2", "value2"));
        Map<String, Object> expectedResult = new HashMap<>();
        expectedResult.put("placeholder1", "value1");
        expectedResult.put("placeholder2", "value2");
        return Arrays.asList(
                DynamicTest.dynamicTest("Should throw an exception when file path is null.",
                        () -> assertThrows(IllegalArgumentException.class,
                                () -> templateEngine.getPlaceholdersWithValuesFromTxtFile(null))
                ),
                DynamicTest.dynamicTest("Should throw an exception when file is not a file.",
                        () -> assertThrows(IllegalArgumentException.class,
                                () -> templateEngine.getPlaceholdersWithValuesFromTxtFile(tempDir))
                ),
                DynamicTest.dynamicTest("Should throw an exception when file extension is not txt.",
                        () -> assertThrows(IllegalArgumentException.class,
                                () -> templateEngine.getPlaceholdersWithValuesFromTxtFile(csvFile))
                ),
                DynamicTest.dynamicTest("Should throw an exception when last placeholder doesn't have value.",
                        () -> assertThrows(IllegalArgumentException.class,
                                () -> templateEngine.getPlaceholdersWithValuesFromTxtFile(file1))
                ),
                DynamicTest.dynamicTest("Should throw an exception when placeholder is missed in 2 row.",
                        () -> assertThrows(IllegalArgumentException.class,
                                () -> templateEngine.getPlaceholdersWithValuesFromTxtFile(file2))
                ),
                DynamicTest.dynamicTest("Should return hashMap of placeholders and values.",
                        () -> assertEquals(expectedResult,templateEngine.getPlaceholdersWithValuesFromTxtFile(file3))
                )
        );
    }

    @TestFactory
    Stream<DynamicTest> testGeneratingOfMessageWithDifferentWays(@TempDir Path tempDir) throws IOException {
        Map<String, Object> inputMap = new HashMap<>();
        inputMap.put(CUSTOMER_NAME_PLACEHOLDER, "Brown");
        inputMap.put(ORDER_ID_PLACEHOLDER, "#123");
        inputMap.put(NUMBER_OF_DELIVERY_DAYS_PLACEHOLDER, 3);
        inputMap.put(COMPANY_NAME_PLACEHOLDER, "APPLE INC");
        Path inputFile = tempDir.resolve("file.txt");
        Files.write(inputFile, Arrays.asList(CUSTOMER_NAME_PLACEHOLDER, "Brown",
                ORDER_ID_PLACEHOLDER, "#123",
                NUMBER_OF_DELIVERY_DAYS_PLACEHOLDER, "3",
                COMPANY_NAME_PLACEHOLDER, "APPLE INC"));
        Map<String,Object> inputs = new HashMap<>();
        inputs.put("map",inputMap);
        inputs.put("file",inputFile);
        Stream<DynamicTest> stream1 = inputs.entrySet().stream()
                .filter(entry -> entry.getKey().equals("map"))
                .map(entry -> DynamicTest.dynamicTest(
                        "Should successfully generate message with hashMap",
                        () -> assertDoesNotThrow(() -> templateEngine.generateMessage(template, (Map<String, Object>) entry.getValue()))
                ));
        Stream<DynamicTest> stream2 = inputs.entrySet().stream()
                .filter(entry -> entry.getKey().equals("file"))
                .map(entry -> DynamicTest.dynamicTest(
                        "Should successfully generate message with file",
                        () -> assertDoesNotThrow(() -> templateEngine.generateMessage(template, (Path) entry.getValue()))
                ));
        return Stream.concat(stream1, stream2);
    }

    private void testSuccessfullyGenerateMessage(Map<String, Object> model) {
        String expectedMessage = template.getContent()
                .replace(CUSTOMER_NAME_PLACEHOLDER, model.get(CUSTOMER_NAME_PLACEHOLDER).toString())
                .replace(ORDER_ID_PLACEHOLDER, model.get(ORDER_ID_PLACEHOLDER).toString())
                .replace(NUMBER_OF_DELIVERY_DAYS_PLACEHOLDER, model.get(NUMBER_OF_DELIVERY_DAYS_PLACEHOLDER).toString())
                .replace(COMPANY_NAME_PLACEHOLDER, model.get(COMPANY_NAME_PLACEHOLDER).toString());
        String actualMessage = templateEngine.generateMessage(template, model);
        assertAll(
                () -> assertEquals(-1, actualMessage.indexOf(CUSTOMER_NAME_PLACEHOLDER)),
                () -> assertEquals(-1, actualMessage.indexOf(ORDER_ID_PLACEHOLDER)),
                () -> assertEquals(-1, actualMessage.indexOf(NUMBER_OF_DELIVERY_DAYS_PLACEHOLDER)),
                () -> assertEquals(-1, actualMessage.indexOf(COMPANY_NAME_PLACEHOLDER)),
                () -> assertEquals(expectedMessage, actualMessage)
        );
    }

    private static class TemplateEngineGenerateMessageNullArgumentsProvider implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
            return Stream.of(
                    Arguments.of(null, null),
                    Arguments.of(new Template(), null),
                    Arguments.of(null, new HashMap<String, Object>())
            );
        }
    }

}